public class OLX implements Provider {
    Products products[];
    float sale;

    public OLX(Products[] products, float sale) {
        this.products = products;
        this.sale = sale;
    }

    @Override
    public String search(String name) {
        for (Products product : products) {
            if (product.name.equalsIgnoreCase(name)) {
                return String.format("OLX: %s, price: %.2f", product.name, product.price * (1 - sale));
            }
        }
        return null;
    }
}
