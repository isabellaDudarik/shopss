public interface Provider {
    String search(String name);
}
