public class Generate {
    public static Products[] getProducts() {
        Products products[] = new Products[3];
        products[0] = new Products("nuts", 22.5f);
        products[1] = new Products("banana", 40.8f);
        products[2] = new Products("orange", 65.3f);
        return products;
    }

    public static Market[] getMarket() {
        Market markets[] = new Market[2];
        markets[0] = new Store("Store", getProducts());
        markets[1] = new Supermarket("Supermarket", getProducts(), 0.05f);

        return markets;
    }
    public static OLX getOLX(){return new OLX(getProducts(), 0.5f);}

//    public static Store getStore() {
//        return new Store("Store", getProducts());
//    }
//
//    public static Supermarket getSupermarket() {
//        return new Supermarket("Supermarket", getProducts(), 0.05f);
//    }
}
