public abstract class Market implements Provider{
    String name;
    Products products[];

    public Market(String name, Products[] products) {
        this.name = name;
        this.products = products;
    }
}
