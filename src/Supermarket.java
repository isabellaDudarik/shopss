public class Supermarket extends Market implements Provider {
    float sale;

    public Supermarket(String name, Products[] products, float upsale) {
        super(name, products);
        this.sale = upsale;
    }

    @Override
    public String search(String name) {
        for (Products product : products) {
            if (product.name.equalsIgnoreCase(name)) {
                return String.format("Supermarket: %s, price: %.2f", product.name, product.price * (1 - sale));
            }
        }
        return null;
    }
}
