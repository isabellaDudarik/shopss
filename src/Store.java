public class Store extends Market implements Provider {
    public Store(String name, Products[] products) {
        super(name, products);
    }

    @Override
    public String search(String name) {
        for (Products product : products) {
            if (product.name.equalsIgnoreCase(name)) {
                return String.format("Store: %s, price: %.2f", product.name, product.price);
            }
        }
        return null;
    }
}
