import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter product: ");
        String product = in.nextLine();

        Market[] markets = Generate.getMarket();
        for (Market market : markets) {
            String price = market.search(product);
            if (price == null) {
                System.out.println(String.format("%s: not found", market.name));
            } else System.out.println(price);
        }

        OLX olx = Generate.getOLX();
        String price = olx.search(product);
        if (price == null) {
            System.out.println(String.format("OLX: not found"));
        } else System.out.println(price);
    }
}

